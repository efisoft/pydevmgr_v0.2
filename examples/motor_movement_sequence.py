""" pydevmgr_elt example 

An application cycling motor position and writing the position in STDOUT  
"""
from pydevmgr_elt import Motor, DataLink, NodeVar, LocalTimeNode, wait
from pydantic import BaseModel, Field
import yaml
import time


#   ____ ___  _   _ _____ ___ ____ 
#  / ___/ _ \| \ | |  ___|_ _/ ___|
# | |  | | | |  \| | |_   | | |  _ 
# | |__| |_| | |\  |  _|  | | |_| |
#  \____\___/|_| \_|_|   |___\____|
###################################

# We are not using a configuration file for the example purpose, the yaml string bellow can 
# be instead red from one config file (or two, one for motor, one for the sequence for instance)

cfg_txt = """
motor:
    prefix: MAIN.motor1
    address: opc.tcp://192.168.1.13:4840
    ctrl_config:
        brake: false
        backlash: 0.003
        # etc ...
sequence:
   positions: [-2.0, 2.0]  # list of positions 
   velocity: 1.0
   pause: 3     # pause after each measurements [second] 
   npoints: 12  # number of measurements in this example 6 measures per position
   init_at_start : false
   init_each_time : false
"""

# Create a config class for the sequence program.
# using a pydantic BaseModel  allows front-end data verification, usefull when the 
# configuration is comming from untrusted source (like config file)
class SequenceConfig(BaseModel):
    positions: list = [0.0, 1.0]
    velocity: float = 1.0    
    pause: float    = Field(0.0, ge=0.0) # pause in between movements
    npoints: int    = 10        # number of measurements 
    
    init_each_time: bool = False # init each time before a movement for instance to check the init sequence repeatability 
    init_at_start: bool = False  # init the motor when program is starting 
    
class AppConfig(BaseModel):
    motor: Motor.Config = Motor.Config()
    sequence: SequenceConfig = SequenceConfig()

#  ____    _  _____  _      __  __  ___  ____  _____ _     
# |  _ \  / \|_   _|/ \    |  \/  |/ _ \|  _ \| ____| |    
# | | | |/ _ \ | | / _ \   | |\/| | | | | | | |  _| | |    
# | |_| / ___ \| |/ ___ \  | |  | | |_| | |_| | |___| |___ 
# |____/_/   \_\_/_/   \_\ |_|  |_|\___/|____/|_____|_____| 
###########################################################

## Now we can create a Data Model for all data used in the app 
# Here we have only one motor from pydevmgr_elt the goal here would be to create some custom nodes 
#  to load data from a serial communication connect to a position sensor for instance 
# 
# The pydevmgr NodeVar is an annotation used by DataLink. It tells the DataLink that the var data 
# shall be taken from the linked object. In the case bellow the AppData can be linked to a motor.stat 
class AppData(BaseModel):
    pos_actual: NodeVar[float] = 0.0
    pos_error: NodeVar[float] = 0.0  
    time: NodeVar[str] = Field('', node=LocalTimeNode('utc'))
       
    index: int = -1        
    measurements: list = [] # save all measurements in a list


#     _    ____  ____  
#    / \  |  _ \|  _ \ 
#   / _ \ | |_) | |_) |
#  / ___ \|  __/|  __/ 
# /_/   \_\_|   |_|    
#######################

def update(data):
    """ This is appending the new data to a list and printing them out for example purpose """        
    data.measurements.append( (data.time, data.pos_actual, data.pos_error) )
    print( data.index, data.time.isoformat(), data.pos_actual, data.pos_error)
    # Do stuff more clever here 

def run(motor, cfg, data):
        
    dl = DataLink(motor.stat, data)
    npos = len(cfg.positions)
    
    if cfg.init_at_start:
        wait(motor.reset())
        wait(motor.init())
        wait(motor.enable())
    
    if not npos:
        return 
        
    while data.index<cfg.npoints:
        data.index += 1
        new_pos = cfg.positions[data.index%npos]
         
        if cfg.init_each_time:
            wait(motor.reset())
            wait(motor.init())
            wait(motor.enable())
            
        wait( motor.move_abs(new_pos, cfg.velocity) )        
        
        dl.download() # download nodes from server and update the data         
        update(data)    
        
        time.sleep(cfg.pause)            
        


## #######################################################################

if __name__ == "__main__":
    # replace the yaml loader to a input argument file for instance     
    cfg = AppConfig.parse_obj( yaml.load(cfg_txt, Loader=yaml.CLoader) )
    data = AppData()
        
    motor = Motor('motor', cfg.motor)
    try:
        # connect to OPC-US
        motor.connect() 
        # send configuration to the device on PLC. Only the parameters edited in the input configuration
        # are updated
        motor.configure()
        run(motor, cfg.sequence, data)
    finally:
        motor.disconnect()
        # you may want to save data.measurements on a file for instance 
        

