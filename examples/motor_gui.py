""" Example of use of pydevmgr 

A Gui to control and configure one motor with live plot of motor positions 

For this we will use the builtin Ctrl and Cfg widget and create a new Widget "WidgetLinker"  

"""

from pydevmgr_elt import Motor, Downloader, NodeVar, DequeNode, LocalTimeNode, UnixTimeNode
from pydevmgr_qt import MotorCtrl, MotorCfg, BaseUiLinker

from pydantic import BaseModel, Field
from PyQt5 import  QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QPushButton, QTabWidget
import numpy as np

import pyqtgraph as pg
import sys


#   ____ ___  _   _ _____ ___ ____ 
#  / ___/ _ \| \ | |  ___|_ _/ ___|
# | |  | | | |  \| | |_   | | |  _ 
# | |__| |_| | |\  |  _|  | | |_| |
#  \____\___/|_| \_|_|   |___\____|
###################################


# We are not using a configuration file here but directly the Config class 
# For a real app most probably the configuration file is an shell argument input 

motor_cfg = Motor.Config(
              address="opc.tcp://192.168.1.13:4840", 
              prefix="MAIN.fsel.motor1", 
              ctrl_config=dict(velocity=3.0, axis_type="CIRCULAR", )#etc...
            )


############################################################
# First we will create the Plot Widget 
# we need three components:
#   - a Data Model declaring all data used by the widget 
#   - a Widget holding the passive QT widget layout 
#   - a Widget Linker (BaseUiLinker) which will link the widget (labels, buttons, etc) to the motor instance 


#  ____    _  _____  _      __  __  ___  ____  _____ _     
# |  _ \  / \|_   _|/ \    |  \/  |/ _ \|  _ \| ____| |    
# | | | |/ _ \ | | / _ \   | |\/| | | | | | | |  _| | |    
# | |_| / ___ \| |/ ___ \  | |  | | |_| | |_| | |___| |___ 
# |____/_/   \_\_/_/   \_\ |_|  |_|\___/|____/|_____|_____|
############################################################
                                                         

# create a node returning the Unix  time 
time = UnixTimeNode('time')


class MotorPlotStatData(BaseModel):
    """ a Data Model defining what we need for the plot widget """
    substate_txt : NodeVar[int] = 0
    plotdata: NodeVar[list] = Field([], node=DequeNode.prop('plotdata', [time, 'pos_actual', 'pos_error', 'is_moving'], maxlen=100, trigger_index=-1)) 
    
class MotorPlotData(BaseModel):
    StatData = MotorPlotStatData
    stat: StatData = StatData()

#  ____  _     ___ _____  __        _____ ____   ____ _____ _____ 
# |  _ \| |   / _ \_   _| \ \      / /_ _|  _ \ / ___| ____|_   _|
# | |_) | |  | | | || |    \ \ /\ / / | || | | | |  _|  _|   | |  
# |  __/| |__| |_| || |     \ V  V /  | || |_| | |_| | |___  | |  
# |_|   |_____\___/ |_|      \_/\_/  |___|____/ \____|_____| |_|  
##################################################################
    
# We create a widget holding the two plots 
class MotorPlotWidget(QWidget):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
                
        layout = QVBoxLayout()
        self.setLayout(layout)
        
        # plots
        g_layout = QHBoxLayout()
        plots = {
          'pos' :       pg.PlotWidget(title='Position'), 
          'pos_error' : pg.PlotWidget(title='Error'), 
        }
        g_layout.addWidget(plots['pos'])
        g_layout.addWidget(plots['pos_error'])        
        layout.addLayout(g_layout)
        
        # a Clear button 
        clear_button = QPushButton()
        clear_button.setText('Clear')
        layout.addWidget(clear_button)
        
        self.clear_button = clear_button
        self.plots = plots


class MotorPlot(BaseUiLinker):
    Data = MotorPlotData   # constructor for Data 
    Widget = MotorPlotWidget # constructor for the default Widget 
    
    def init_vars(self):
        # the goal of init vars is to collect, grap the outputs and inputs information 
        # of the widget 
        self.outputs.plots = self.widget.plots
        
    def update(self, data: MotorPlotData) -> None:
        """ Update the widget (the plots in our case) """
        plotdata = np.asarray(data.stat.plotdata)
        if len(plotdata):
            p = self.outputs.plots['pos']
            p.clear()        
            p.plot(plotdata[:,1])
            
            p = self.outputs.plots['pos_error']
            p.clear()        
            p.plot(plotdata[:,2])
    
    # a connect_device is only needed when one have to set inputs action 
    # like button .. etc ... in this case the only action is clear (which is actually not related 
    #   to the device but the data)
    def connect_device(self, device, data):
        
        def clear_plotdata():
            data.stat.plotdata.clear()
            self.outputs.plots['pos'].clear()
            self.outputs.plots['pos_error'].clear()
        
        # we could also do self.widget.clear_button.clicked.connect(clear_plotdata)
        # but the actions object allow to collect all widget connection and unlink them properly 
        # when the window is closed for instance
        self.actions.add(clear_plotdata).connect_button(self.widget.clear_button)            
        
    
#   ____ _   _ ___ 
#  / ___| | | |_ _|
# | |  _| | | || | 
# | |_| | |_| || | 
#  \____|\___/|___|
#################### 

def app_main(motor):
    """ main gui window accepting a motor device instance 
    
    the motor shall be connected
    """
    
    downloader = Downloader()
    app = QApplication(sys.argv)
    
    main_widget = QWidget()
    main_layout = QHBoxLayout()
    main_widget.setLayout(main_layout)
    
    
    # Initialize tab screen
    tabs = QTabWidget()
    tab_ctrl   = QWidget()
    tab_config = QWidget()
    #tabs.resize(300,200)
        
    # Add tabs
    tabs.addTab(tab_ctrl  ,"Control")
    tabs.addTab(tab_config,"Config")
    
    
    layout_ctrl = QVBoxLayout()
    tab_ctrl.setLayout(layout_ctrl)
    
    layout_config = QVBoxLayout()
    tab_config.setLayout(layout_config)
    
    # Build a MotorCtrl and connect it to the downloader and motor         
    motor_ctrl = MotorCtrl().connect(downloader, motor)
    layout_ctrl.addWidget(motor_ctrl.widget)
    
    motor_plot = MotorPlot().connect(downloader, motor)
    layout_ctrl.addWidget(motor_plot.widget)
    
    motor_cfg = MotorCfg().connect(downloader, motor)
    layout_config.addWidget(motor_cfg.widget)
    
    #main_layout.addWidget(left_widget)
    main_layout.addWidget(tabs)
    
    main_widget.show()
            
    # To refresh the gui we need a timer and connect the download method 
    timer = QtCore.QTimer()
    timer.timeout.connect(downloader.download)
    # 10Hz GUI is nice
    timer.start(100)
        
    return app.exec_()



## ############################################################

if __name__ == "__main__":
    motor = Motor('motor', motor_cfg)
    
    try:
        motor.connect()
        app_main(motor)
    finally:
        motor.disconnect()
